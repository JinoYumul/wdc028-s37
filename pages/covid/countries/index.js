import { ListGroup } from 'react-bootstrap';
import Link from 'next/link';

export default function index({data}){

  const countriesStats = data.countries_stat
  const countriesList = countriesStats.map(country => {
    return (
        <ListGroup.Item key={country.country_name}>
          <Link href={`/covid/countries/${country.country_name.replace(/\s/g,"")}`}>
             <a>
               {country.country_name}
             </a>
          </Link>
        </ListGroup.Item>
      )
  })

  return (
    <ListGroup>
      {countriesList}
    </ListGroup>
  )
}

export async function getStaticProps(){
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "f5863d91d8msh70c2e6a438521f8p1d5c7ajsn13535d52a07e"
    }
  })

  const data = await res.json()

  return {
    props: {
      data
    }
  }
}