import toNum from '../../../toNum'
import DoughnutChart from '../../../components/DoughnutChart'
import Banner from '../../../components/Banner'

export default function country({ country }){
  return (
    <React.Fragment>
      <Banner country={country.country_name} cases={country.cases} deaths={country.deaths} criticals={country.serious_critical} recoveries={country.total_recovered} />
      <DoughnutChart cases={toNum(country.cases)} criticals={toNum(country.serious_critical)} deaths={toNum(country.deaths)} recoveries={toNum(country.total_recovered)}/>    
    </React.Fragment>
  )
}

export async function getStaticPaths(){
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
  "method": "GET",
  "headers": {
    "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
    "x-rapidapi-key": "f5863d91d8msh70c2e6a438521f8p1d5c7ajsn13535d52a07e"
    }
  })

  const data = await res.json()

  const paths = data.countries_stat.map(country => ({
    params: { id: country.country_name.replace(/\s/g,"") }
  }))

  return { paths, fallback: false}
}

export async function getStaticProps({params}){
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "f5863d91d8msh70c2e6a438521f8p1d5c7ajsn13535d52a07e"
    }
  })

  const data = await res.json()

  const country = data.countries_stat.find(country => country.country_name.replace(/\s/g,"") === params.id)

  return {
    props: {
      country
    }
  }
}