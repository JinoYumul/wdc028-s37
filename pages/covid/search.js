import { useState, useRef, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY
import DoughnutChart from '../../components/DoughnutChart';
import toNum from '../../toNum';

export default function Home({data}){
	const countriesStats = data.countries_stat
	const [targetCountry, setTargetCountry] = useState('')
	const [name, setName] = useState('')
	const [cases, setCases] = useState(0)
	const [criticals, setCriticals] = useState(0)
	const [deaths, setDeaths] = useState(0)
	const [recoveries, setRecoveries] = useState(0)

	const mapContainerRef = useRef(null)

	const [latitude, setLatitude] = useState(0)
	const [longitude, setLongitude] = useState(0)

	useEffect(() => {
		const map = new mapboxgl.Map({
			container: mapContainerRef.current,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude, latitude],
			zoom: 3
		})

		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)

		return () => map.remove()
	}, [latitude, longitude])

	function search(e){
		e.preventDefault()
		const match = countriesStats.find(country => country.country_name.toLowerCase() === targetCountry.toLowerCase())
		if(match){
			setName(match.country_name)
			setCases(toNum(match.cases))
			setCriticals(toNum(match.serious_critical))
			setDeaths(toNum(match.deaths))
			setRecoveries(toNum(match.total_recovered))

			fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${targetCountry}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
			.then(res => res.json())
			.then(data => {
				setLongitude(data.features[0].center[0])
				setLatitude(data.features[0].center[1])
			})
		}
	}

	return (
		<React.Fragment>
			<Form onSubmit={e => search(e)}>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control type="text" placeholder="Search for a country" value={targetCountry} onChange={e => setTargetCountry(e.target.value)}/>
					<Form.Text className="text-muted">
						Get Covid-19 stats of searched for country.
					</Form.Text>
				</Form.Group>

				<Button variant="primary" type="submit">Submit</Button>
			</Form>

			<h1>Country: {name}</h1>
			<Row>
				<Col xs={12} md={6}>
					<DoughnutChart cases={cases} criticals={criticals} deaths={deaths} recoveries={recoveries}/>
				</Col>
				<Col xs={12} md={6}>
					<div className="mapContainer" ref={mapContainerRef} />
				</Col>
			</Row>
		</React.Fragment>
	)
}

export async function getStaticProps(){
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "f5863d91d8msh70c2e6a438521f8p1d5c7ajsn13535d52a07e"
    }
  })

  const data = await res.json()

  return {
    props: {
      data
    }
  }
}