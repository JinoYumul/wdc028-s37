import toNum from '../toNum';
import { Jumbotron } from 'react-bootstrap';

export default function Home({globalTotal}) {
  return (
    <Jumbotron>
      <h1>Total Covid-19 cases in the world: <strong>{globalTotal.cases}</strong></h1>
    </Jumbotron>
  )
}

export async function getStaticProps(){
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "f5863d91d8msh70c2e6a438521f8p1d5c7ajsn13535d52a07e"
    }
  })

  const data = await res.json()
  const countriesStats = data.countries_stat

  let total = 0
  countriesStats.forEach(country => {
    total += toNum(country.cases) 
  })

  const globalTotal = {
    cases: total
  }

  return {
    props: {
      globalTotal
    }
  }
}