import { Doughnut } from 'react-chartjs-2';

export default function DoughnutChart({ cases, criticals, deaths, recoveries }){
	return (
		<Doughnut data={{
			datasets: [{
				data: [cases, criticals, deaths, recoveries],
				backgroundColor: ["blue", "orange", "red", "green"]
			}],
			labels: ["Cases", "Criticals", "Deaths", "Recoveries"]
		}} redraw={false}/>
	)
}